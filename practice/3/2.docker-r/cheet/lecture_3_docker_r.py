from airflow import DAG
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime

default_args = {
    "owner": "was"
}

with DAG(
    "lecture_3_docker_r",
    start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@hourly",
    catchup=False,
) as dag:

    run_r = DockerOperator(
        task_id="run_r",
        api_version="1.41",
        image="mydocker-r",
        auto_remove=True,
    )

    run_r_args = DockerOperator(
        task_id="run_r_args",
        api_version="1.41",
        image="mydocker-r",
        command="Rscript script_2.R ARG1",
        auto_remove=True,
    )

    run_r_from_registry = DockerOperator(
        task_id="run_r_from_registry",
        api_version="1.41",
        docker_conn_id="docker_gitlab",
        image="gitlab.treeum.net:5005/analytics/airflow-sandbox/docker-r:latest",
        command="Rscript script_2.R {{ds}}",
        force_pull=True
    )

