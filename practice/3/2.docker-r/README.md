# Запускаем контейнер с R скриптами в Airflow

1. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_3_docker_r.py
```

2. Собираем конейтенер c R скриптами

```bash
cd practice/3/2.docker-r
docker build . -t mydocker-r
```

3 Импортируем необходимые библиотеки, операторы и хуки

```python
from airflow import DAG
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime
```

4. Создаем DAG c выполнением каждый час

```python
default_args = {
    "owner": "was"
}

with DAG(
    "lecture_3_docker_r",
    start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@hourly",
    catchup=False,
) as dag:
```

5. Добавляем оператор запуска контейнера

```python
    run_r = DockerOperator(
            task_id="run_r",
            api_version="1.41",
            image="mydocker-r",
            auto_remove=True,
        )
```

6. Запускаем даг из интерфейса и проверяем логи нашей задачи "run_r"

7. Добавляем оператор запуска контейнера с альтернативной командой выполнения скрипта *script_2.R*

```python
    run_r_args = DockerOperator(
        task_id="run_r_args",
        api_version="1.41",
        image="mycontainer",
        command="Rscript script_2.R {{ds}}",
        auto_remove=True,
    )
```

8. Запускаем даг из интерфейса и проверяем логи нашей задачи "run_r_args"

9. Разместим наш контейнер в приватном репозитории. Для этого создадим персональный токен в разделе https://gitlab.treeum.net/-/profile/personal_access_tokens с правами read_registry и write_registry. Полученные `username` и `personal access token` и спользуем для входа

```bash
docker login -u %username% -p %personal access token% gitlab.treeum.net:5005
```

10. Собераем контейнер c другим названием тегом

```bash
docker build -t gitlab.treeum.net:5005/analytics/airflow-sandbox/docker-r:latest . 
```

11. Пушим контейенер в наш приватный репозиторий и проверяем наличие записи в разделе https://gitlab.treeum.net/analytics/airflow-sandbox/container_registry

```bash
docker push gitlab.treeum.net:5005/analytics/airflow-sandbox/docker-r:latest
```

12. Для того чтобы работать с приватными реестрами образов, необходимо использовать Connect с типом Docker. Для этого создадим в интерфейсе коннект `docker_gitlab` и укажием его в DockerOperator

```python
run_r_from_registry = DockerOperator(
        task_id="run_r_from_registry",
        api_version="1.41",
        docker_conn_id="docker_gitlab",
        image="gitlab.treeum.net:5005/analytics/airflow-sandbox/docker-r:latest",
        command="Rscript script_2.R {{ds}}",
        force_pull=True
    )
```

13. Для того чтобы запуск был всегда актуальной версии указываем принудительное скачивание образа перед запуском

```python
run_r_from_registry = DockerOperator(
        task_id="run_r_from_registry",
        ...
        force_pull=True, # Принудительная загрузка докер образа
    )
```