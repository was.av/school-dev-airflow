# Организия ML-pipeline в Airflow

1. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_3_mlpipeline.py
```

2. Импортируем необходимые библиотеки, операторы и хуки

```python
import os.path

from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime

from docker.types import Mount
```

3. Добалвяем переменные c ссылками на тестовые датасеты, обрабатываемые файлы и создаем объект монтирования

```python
train_data_url = "https://raw.githubusercontent.com/rpi-techfundamentals/spring2019-materials/master/input/train.csv"
test_data_url = "https://raw.githubusercontent.com/rpi-techfundamentals/spring2019-materials/master/input/test.csv"

STORAGE = Variable.get("STORAGE", default_var="/workspace") # TODO: change default value

train_data_filename = os.path.join(STORAGE, "train.csv")
test_data_filename = os.path.join(STORAGE, "test.csv")
predict_data_filename = os.path.join(STORAGE, "predict.csv")
model_filename = os.path.join(STORAGE, "model.joblib")

mount_storage = Mount(STORAGE, "/workspace", type="bind") # TODO: change mount paths
```


4. Создаем DAG с единоразовым выполнением

```python
default_args = {
    "owner": "was"
}
with DAG("lecture_3_mlpipeline", start_date=datetime(2021, 11, 1, tz="Europe/Kiev"), schedule_interval="@once", catchup=False) as dag:
```

5. Добавляем Операторы для скачивания датасетов

```python
    fetch_train_data = BashOperator(
        task_id="fetch_train_data",
        bash_command=f"wget {train_data_url} -O {train_data_filename}"
    )

    fetch_test_data = BashOperator(
        task_id="fetch_test_data",
        bash_command=f"wget {train_data_url} -O {test_data_filename}"
    )
```

6. Запускаем DAG в тестовом режиме и проверяем наличие скачанных файлов

```
airflow dags test lecture_3_mlpipeline 2021-11-01
```

7. Собирае конейтенер c нашими скриптами для построения моделей

```bash
cd practice/3/3.mlpipeline
docker build . -t mymodel
```

8. Добавляем задачу запуска Docker контейнера со скриптом построение модели. Для того чтобы файлы были доступны в контейнере, монитруем необходимую директорию

```python
    build_model = DockerOperator(
        task_id="build_model",
        image="mymodel",
        command=f"python train.py --train_filename={train_data_filename} --model_filename={model_filename} --test_size=0.2",
        auto_remove=True,
        mounts=[mount_storage]
    )

    [fetch_train_data, fetch_test_data] >> build_model
```

9. Запускаем DAG в тестовом режиме и проверяем наличие построенной модели

```bash
airflow dags test lecture_3_mlpipeline 2021-11-01
```

10. Добавляем задачу запуска Docker контейнера со скриптом имплементации модели и так же монтируем директорию.

```python
    implement_model = DockerOperator(
            task_id="implement_model",
            image="mymodel",
            command=f"python inference.py --model_filename={model_filename} --in_filename={test_data_filename} --out_filename={predict_data_filename}",
            auto_remove=True,
            mounts=[mount_storage]
        )
```

11. Обновляем цепочку задач

```python
[fetch_train_data, fetch_test_data] >> build_model >> implement_model
```

12. Запускаем DAG в тестовом режиме и проверяем наличие файла с предсказаниями

```bash
airflow dags test lecture_3_mlpipeline 2021-11-01
```