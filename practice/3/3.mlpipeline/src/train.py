import argparse
import joblib

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, f1_score

# read dataframe
# split datarame (df, test_ratio, random_state)


def train(train_filename, test_size):
    df = pd.read_csv(train_filename)
    # features
    df = df[['Pclass','Sex','Age', 'Survived']]
    df.loc[df['Sex']=='male','Sex']=0
    df.loc[df['Sex']=='female','Sex']=1
    # fill na
    df.fillna(0, inplace=True)

    y = df['Survived']
    del df['Survived']

    X_train, X_test, y_train, y_test = train_test_split(df, y, test_size=test_size, random_state=42)

    model = RandomForestClassifier(
        n_estimators=50,
        random_state=10
    )
    model.fit(X_train, y_train)

    train_predict_probas = model.predict_proba(X_train)[:,1]
    train_roc_auc = roc_auc_score(y_true=y_train, y_score=train_predict_probas)
    train_f1 = f1_score(y_true=y_train, y_pred=train_predict_probas>0.5)
    print(f"TRAIN:\n\tROC_AUC: {train_roc_auc}\n\tF1: {train_f1}") 

    test_predict_probas= model.predict_proba(X_test)[:,1]
    test_roc_auc = roc_auc_score(y_true=y_test, y_score=test_predict_probas)
    test_f1 = f1_score(y_true=y_test, y_pred=test_predict_probas>0.5)
    print(f"TEST:\n\tROC_AUC: {test_roc_auc}\n\tF1: {test_f1}") 

    # TODO: save as model_score.json

    return model
    

def main():
    parser = argparse.ArgumentParser(description='Train test split.')
    parser.add_argument('--train_filename', required=True, help='Input fileme with features for split.')
    parser.add_argument('--model_filename', help='Input fileme with features for split.')
    parser.add_argument('--test_size', type=float, help='Test size 0> - <1.0.')

    args = parser.parse_args()
    print(args)

    model = train(args.train_filename, args.test_size)
    joblib.dump(model, args.model_filename)



if __name__ == '__main__':
    main()
    