import argparse

import joblib
import pandas as pd

def inference(model, df):
    df = df[['Pclass','Sex','Age']]
    df.loc[df['Sex']=='male','Sex']=0
    df.loc[df['Sex']=='female','Sex']=1
    # fill na
    df.fillna(0, inplace=True)
    predicts= model.predict(df)
    return predicts

def main():
    parser = argparse.ArgumentParser(description='Train test split.')
    parser.add_argument('--model_filename', help='Model filename.')
    parser.add_argument('--in_filename', help='Input test filename.')
    parser.add_argument('--out_filename', help='Output with predition filename.')

    args = parser.parse_args()
    print(args)

    df = pd.read_csv(args.in_filename)
    passanger_id = df['PassengerId']
    model = joblib.load(args.model_filename)
    predicts = inference(model, df)
    pd.DataFrame({"PassengerId": passanger_id, "Survived": predicts}).to_csv(args.out_filename, index=False)

if __name__ == '__main__':
    main()
    