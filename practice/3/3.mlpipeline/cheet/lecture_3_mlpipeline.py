import os.path

from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime

from docker.types import Mount

train_data_url = "https://raw.githubusercontent.com/rpi-techfundamentals/spring2019-materials/master/input/train.csv"
test_data_url = "https://raw.githubusercontent.com/rpi-techfundamentals/spring2019-materials/master/input/test.csv"

STORAGE = Variable.get("STORAGE", default_var="/workspace")

train_data_filename = os.path.join(STORAGE, "train.csv")
test_data_filename = os.path.join(STORAGE, "test.csv")
predict_data_filename = os.path.join(STORAGE, "predict.csv")
model_filename = os.path.join(STORAGE, "model.joblib")

mount_storage = Mount(STORAGE, "/workspace", type="bind")

default_args = {
    "owner": "was"
}
with DAG("lecture_3_mlpipeline", start_date=datetime(2021, 11, 1, tz="Europe/Kiev"), schedule_interval="@once", catchup=False) as dag:
    fetch_train_data = BashOperator(
        task_id="fetch_train_data",
        bash_command=f"wget {train_data_url} -O {train_data_filename}"
    )

    fetch_test_data = BashOperator(
        task_id="fetch_test_data",
        bash_command=f"wget {train_data_url} -O {test_data_filename}"
    )

    build_model = DockerOperator(
        task_id="build_model",
        image="mymodel",
        command=f"python train.py --train_filename={train_data_filename} --model_filename={model_filename} --test_size=0.2",
        auto_remove=True,
        mounts=[mount_storage]
    )

    implement_model = DockerOperator(
        task_id="implement_model",
        image="mymodel",
        command=f"python inference.py --model_filename={model_filename} --in_filename={test_data_filename} --out_filename={predict_data_filename}",
        auto_remove=True,
        mounts=[mount_storage]
    )

    [fetch_train_data, fetch_test_data] >> build_model >> implement_model
