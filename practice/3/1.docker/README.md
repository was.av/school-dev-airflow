# Запуск контейнеров в Airflow

1. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_3_docker.py
```

2 Импортируем необходимые библиотеки, операторы и хуки

```python
from airflow import DAG
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime
from docker.types import Mount
```

4. Создаем DAG c без регламентного выполнения (только ручное)

```python
default_args = {
    "owner": "was"
}

with DAG(
    "lecture_3_docker",
    start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval=None,
    catchup=False,
) as dag:
```

5. Добалвяем запуск Docker контейнера hello-world c самоунчтожением используя DockerOperator

```python
    run_container = DockerOperator(
        task_id="run_container",
        api_version="1.41",
        image="hello-world",
        auto_remove=True,
    )
```

6. Добавляем контейнеру динамическое название и рамещаем его в сети хост-машины

```python
    run_container_name = DockerOperator(
        task_id="run_container_name",
        api_version="1.41",
        image="hello-world",
        auto_remove=True,
        container_name="hello-world__{{ ds }}",
        network_mode="host",
    )
```

7. Добавляем запуск контейнера c ОС Debian с пользовательской командой `date`

```python
    run_debian_command = DockerOperator(
        task_id="run_debian_command",
        api_version="1.41",
        image='debian:latest',
        auto_remove=True,
        command='date',
    )
```

8. Для передачи параметров в команду запускаемую контейнером можем воспользоваться переменными окружениями общими и приватными (не будут отображены в интерфесе airflow)

```python
    run_debian_env = DockerOperator(
        task_id="run_debian_env",
        api_version="1.41",
        image='debian:latest',
        auto_remove=True,
        command='env',
        environment={
            "ENV1": "E1-V1",
        },
        private_environment={
            "PRIVATE_ENV1": "PE1-V1",
        }
    )
```

9. Для обмена файлами с программами запускаемыми в Docker контейнерах используется монтирование томов (volumms). Примонтируем директорию проекта и выведем содержимое нашего дага в консоль

```python
run_debian_mount = DockerOperator(
        task_id="run_debian_mount",
        docker_conn_id="",
        api_version="1.41",
        image='debian:latest',
        command='cat /workspaces/school-dev-airflow-mounted/dags/lecture_3_docker.py',
        auto_remove=True,
        mounts=[
            Mount("/workspaces/school-dev-airflow-mounted",
                  "/workspace/school-dev-airflow", type="bind")
        ],
    )
```
