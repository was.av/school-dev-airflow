from airflow import DAG
from airflow.providers.docker.operators.docker import DockerOperator
from pendulum import datetime
from docker.types import Mount

default_args = {
    "owner": "was"
}

with DAG(
    "lecture_3_docker",
    start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval=None,
    catchup=False,
) as dag:

    run_container = DockerOperator(
        task_id="run_container",
        api_version="1.41",
        image="hello-world",
        auto_remove=True,
    )

    run_container_name = DockerOperator(
        task_id="run_container_name",
        api_version="1.41",
        image="hello-world",
        auto_remove=True,
        container_name="hello-world__{{ ds }}",
        network_mode="host",
    )

    run_debian_command = DockerOperator(
        task_id="run_debian_command",
        api_version="1.41",
        image='debian:latest',
        auto_remove=True,
        command='date',
    )

    run_debian_env = DockerOperator(
        task_id="run_debian_env",
        api_version="1.41",
        image='debian:latest',
        command='env',
        auto_remove=True,
        environment={
            "ENV1": "E1-V1",
        },
        private_environment={
            "PRIVATE_ENV1": "PE1-V1",
        }
    )

    run_debian_mount = DockerOperator(
        task_id="run_debian_mount",
        api_version="1.41",
        image='debian:latest',
        command='cat /workspaces/school-dev-airflow-mounted/dags/lecture_3_docker.py',
        auto_remove=True,
        mounts=[
            Mount("/workspaces/school-dev-airflow-mounted",
                  "/workspace", type="bind")
        ],
    )