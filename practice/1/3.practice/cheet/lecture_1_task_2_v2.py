import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator

default_args = {
    "owner": "was"
}

def compare_covid_confirm_func(ti):
    import json

    covid_data = json.loads(ti.xcom_pull(
        task_ids="extract_covid_data", key="return_value"))

    covid_data_prev = json.loads(ti.xcom_pull(
        task_ids="extract_covid_data_prev", key="return_value"))

    regions = Variable.get("covid_region", "Dnipropetrovska")
    confirmed_prev = Variable.get(
        "covid_region_confirmed_prev", 0)
    message = ""
    for region in covid_data["ukraine"]:
        if region["label"]["en"] in regions \
                or region["label"]["uk"] in regions:
            confirmed = int(region["confirmed"])            
            confirmed_prev = [r for r in covid_data_prev["ukraine"] if r["id"] == region["id"]][0]["confirmed"]
            change = confirmed - confirmed_prev
            change = f"+{change}" if change > 0 else str(change)
            message += f"Подтвержденные случаи COVID в {region['label']['uk']} - {confirmed} ({change})\n"

    return message.strip()


with DAG("lecture_1_task_2_v2",
         default_args=default_args,
         schedule_interval="@daily",
         start_date=pendulum.datetime(2021, 10, 10, tz="Europe/Kiev"),
         catchup=False,
         ) as dag:


    extract_covid_data = SimpleHttpOperator(
        task_id="extract_covid_data",
        method='GET',
        http_conn_id="http_covid",
        endpoint="/data",
        data={"to": "{{ ds }}"},
        headers={"Content-Type": "application/json"},
        log_response=True,
    )

    extract_covid_data_prev = SimpleHttpOperator(
        task_id="extract_covid_data_prev",
        method='GET',
        http_conn_id="http_covid",
        endpoint="/data",
        data={"to": "{{ macros.ds_add(ds, -1) }}"},
        headers={"Content-Type": "application/json"},
        log_response=True,
    )

    compare_covid_confirm = PythonOperator(
        task_id="compare_covid_confirm",
        python_callable=compare_covid_confirm_func
    )

    send_message = BashOperator(
        task_id="send_message",
        bash_command="""\
            echo "{{ ti.xcom_pull(task_ids="compare_covid_confirm", key="return_value") }}"
            """
    )

    [extract_covid_data, extract_covid_data_prev] >> compare_covid_confirm >> send_message
