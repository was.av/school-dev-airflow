from airflow import DAG
from airflow.operators.dummy import DummyOperator
import pendulum

default_args = {
    "owner": "was"
}

with DAG("lecture_1_task_1",
         default_args=default_args,
         schedule_interval="@daily",
         start_date=pendulum.datetime(2021, 10, 10, tz="Europe/Kiev"),
         catchup=False,
         ) as dag:
    start = DummyOperator(task_id="start")

    section_1 = []
    for i in range(1, 6):
        section_1.append(DummyOperator(task_id=f"section-1-tasks-{i}"))

    some_other_task = DummyOperator(task_id="some-other-task")

    section_2 = []
    for i in range(1, 6):
        section_2.append(DummyOperator(task_id=f"section-2-tasks-{i}"))

    end = DummyOperator(task_id="end")

    start >> section_1 >> some_other_task >> section_2 >> end
