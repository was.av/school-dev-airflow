import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator

default_args = {
    "owner": "was"
}


def extract_covid_data_python_func(ds):
    import requests
    return requests.get(f"https://api-covid19.rnbo.gov.ua/data?to={ds}").text
    # import json

    # with open('dags/covid_data.json') as f:
    #     return json.dumps(json.load(f))


def compare_covid_confirm_func(ti):
    import json

    covid_data = json.loads(ti.xcom_pull(
        task_ids="extract_covid_data_python", key="return_value"))

    regions = Variable.get("covid_region", "Dnipropetrovska")
    confirmed_prev = Variable.get(
        "covid_region_confirmed_prev", 0)
    message = ""
    for region in covid_data["ukraine"]:
        if region["label"]["en"] in regions \
                or region["label"]["uk"] in regions:
            confirmed = int(region["confirmed"])
            confirmed_key = f"covid_region_{region['label']['en']}_confirmed_prev"
            confirmed_prev = int(Variable.get(confirmed_key, 0))
            change = confirmed - confirmed_prev
            change = f"+{change}" if change > 0 else str(change)
            message += f"Подтвержденные случаи COVID в {region['label']['uk']} - {confirmed} ({change})\n"            
            Variable.set(confirmed_key, confirmed)

    return message.rstrip()


with DAG("lecture_1_task_2",
         default_args=default_args,
         schedule_interval="@daily",
         start_date=pendulum.datetime(2021, 10, 10, tz="Europe/Kiev"),
         catchup=False,
         ) as dag:

    extract_covid_data_python = PythonOperator(
        task_id="extract_covid_data_python",
        python_callable=extract_covid_data_python_func
    )

    compare_covid_confirm = PythonOperator(
        task_id="compare_covid_confirm",
        python_callable=compare_covid_confirm_func
    )

    send_message = BashOperator(
        task_id="send_message",
        bash_command="""\
            echo "{{ ti.xcom_pull(task_ids="compare_covid_confirm", key="return_value") }}"
            """
    )

    extract_covid_data_python >> compare_covid_confirm >> send_message
