# DAGs

## 1. Создадим простой DAG

Создаем новый фацл simpledag.py в директории dags

Импортируем необходимый необходимые модули 

```python
from datetime import datetime

from airflow import DAG
from airflow.operators.bash import BashOperator
```

Описываем аргументы по умолчанию

```python
default_args = {
    "owner": "author login" // Логин влайдельца
}
```

Создаем DAG

```python
dag = DAG(
    "simpledag",                        // название DAG, если в файле 1 DAG, то указываем название файла
    default_args=default_args,          // аргументы по умолчанию
    start_date=datetime(2021, 11, 1),   // дата старта
    )
```

Добавляем задача на основе BashOperator

```python
t1 = BashOperator(
    task_id="t1",                       // название задачи
    dag=dag,                            // DAG в рамках которого будет выполнена задача
    bash_command="date"                 // bash команда
    )
```

Выводим список всех DAG

```bash
airflow dags list
```

Результат должен быть следующим

```bash
dag_id    | filepath     | owner   | paused
==========+==============+=========+=======
simpledag | simpledag.py | was     | None  
```

Выводим список всех задач для simpledag

```bash
airflow tasks list simpledag -t
```

Результат должен быть следующим

```bash
<Task(BashOperator): t1>
```

## 2. Запуск и отладка DAG

Запускаем DAG следующей командой

```python
airflow dags test simpledag 2021-11-1
```

Добавляем конфигурацию для отладки dag.
Переходим в раздел *Debug and Run* и нажимаем на *create a launch.json*.
В выпадающем списке выбираем *Python* и *Python File*


Отроется файл *lunch.json* со следующим содержимым

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current File",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal"
        }
    ]
}
```

Добавляем конфигураию запуска DAG в режиме теста

```json
        {
            "name": "Airflow Test: Current File",
            "type": "python",
            "request": "launch",
            "module": "airflow",
            "args": ["dags", "test", "${fileBasenameNoExtension}", "2021-12-31"],
            "console": "integratedTerminal",
            "env": {
                "AIRFLOW__CORE__EXECUTOR": "DebugExecutor",
                "AIRFLOW__DEBUG__FAIL_FAST": "True"
            }
        }
```

В выпадающем списке выбираем конфигурацию отладки *Airflow Test: Current File*