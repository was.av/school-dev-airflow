from datetime import datetime
import json

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator

from airflow.models import Variable

Variable.set('my_var', 'v1')
Variable.get('my_var_2', default_var="v2")
Variable.set('my_json', '{"p2": "v2"}')

default_args = {
    "owner": "was",
}

dag = DAG(
    "simpledag",
    default_args=default_args,
    start_date=datetime(2021, 11, 1),
    schedule_interval="@hourly",
    catchup=False,
    tags=["school-dev"])

t1 = BashOperator(
    task_id="t1",
    dag=dag,
    bash_command="date"
)
t1.doc_md = """ \
### Task description
"""


t2 = BashOperator(task_id="t2", dag=dag, bash_command="""\
{% for i in range(3) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, i)}}"
        echo "{{ var.value.my_var }}"
{% endfor %}
""")
t1.set_downstream([t2])

def my_function(ti, **kwargs):
    p1 = kwargs['params']['p1']
    print(f"P1: {p1}")
    my_json_str = Variable.get("my_json")
    my_json = json.loads(my_json_str)
    p2 = my_json["p2"]
    print(f"P2: {p2}")

    ti.xcom_push('p3', "v3")


t3 = PythonOperator(
    task_id="t3",
    dag=dag,
    python_callable=my_function,
    params={"p1": "v1'"}
)

t2 >> t3

t4 = BashOperator(task_id="t4", dag=dag, bash_command="""\
    echo "P3: {{ ti.xcom_pull(task_ids='t3', key='p3') }}"
""")
t2 >> t4

t5 = DummyOperator(task_id='t5')
[t3, t4] >> t5