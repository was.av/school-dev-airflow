# Установка и первый запуск

## 1. Проверяем установленный airflow

Выполняем команду

```bash
airflow version
```

Результат должен быть следующим

```bash
2.2.2
```

Видим новые файлы:

* airflow.cfg - файл конфигурацаии 
* weserver_config.py - файл конфигурации Web- сервера

## 1. Запускаем Airflow в режиме standalone

Ининициализуем базу, пользователя и запускаем все компоненты

```bash
airflow standalone
```

Видим новые файлы:

* airflow.db - SQLite база метаданных
* standalone_admin_password.txt - файл с паролем пользователя admin

### *1.1 Изменяем порты веб-сервера и планировщика (только в случае конфликта портов)

Базовые значения
web_server_port = 8080
worker_log_server_port = 8793
base_url = http://localhost:8080
endpoint_url = http://localhost:8080

Заменяемые значения из [таблицы](https://docs.google.com/spreadsheets/d/1qteWe-lRVmqYT4SnMg_3-8abKz2BJ7HvthxbQEo1J0I/edit#gid=0)

