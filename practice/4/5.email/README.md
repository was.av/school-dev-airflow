# E-mail уведомления

1. Стандартные нотификации на почту при падени

    1.1 Регистриуемся в сервисе mailtrap.io (можно использовать google аккаунт) и сохраняем логин и пароль (login, password)

    1.2 Создаем Connection smtp_default и заполняем полученными данными и заполняем
        
        Connection Type: Email
        Host: smtp.mailtrap.io
        Schema: smtp
        Port: 2525
        Login: login
        Password: password

    1.3 Создаем наш будущий DAG

    ```bash
    touch dags/lecture_4_notification_email.py
    ```

    1.4 Импортируем необходимые зависимости

    ```python
    from functools import partial

    from airflow import DAG
    from airflow.exceptions import AirflowFailException
    from airflow.operators.python import PythonOperator
    from pendulum import datetime
    ```

    1.5 Описываем параметр по умолчанию и в качестве параметра email указываем свой логин

    ```python
        default_args = {
            "owner": "was",
            'depends_on_past': False,
            "email": ["wasrull@gmail.com"],
            "email_on_failure": True,
            "email_on_retry": True,
        }
    ```

    1.6 создаем даг c оператором который завершится ошибкой

    ```python
    def my_fail_func():
        raise AirflowFailException("My Error")

    with DAG("lecture_4_notification_email",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False,
         ) as dag:
        fail = PythonOperator(task_id="fail_func", python_callable=my_fail_func)
    ```

    1.7 Запускаем DAG, который завершится ошибкой и проверяем сообщение в телеграмм канале