from functools import partial

from airflow import DAG
from airflow.exceptions import AirflowFailException
from airflow.operators.python import PythonOperator
from pendulum import datetime


def my_fail_func():
    raise AirflowFailException("My Error")


default_args = {
    "owner": "was",
    'depends_on_past': False,
    "email": ["wasrull@gmail.com"],
    "email_on_failure": True,
    "email_on_retry": True,
}

with DAG("lecture_4_notification_email",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False,
         ) as dag:
    fail = PythonOperator(task_id="fail_func", python_callable=my_fail_func)
