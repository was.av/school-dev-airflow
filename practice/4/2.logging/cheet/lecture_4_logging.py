import logging

from airflow import DAG
from airflow.operators.python import PythonOperator
from pendulum import datetime


def my_func():
    logging.debug("Log with Level Debug")
    logging.info("Log with Level Info")
    logging.warning("Log with Level Warning")
    logging.error("Log with Level Error")
    logging.critical("Log with Level Critical")


default_args = {
    "owner": "was"
}

with DAG("lecture_4_logging",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False) as dag:

    set_var = PythonOperator(
        task_id="log",
        python_callable=my_func)
