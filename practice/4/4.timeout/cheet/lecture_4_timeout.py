import logging
from functools import partial

from airflow import DAG
from airflow.exceptions import AirflowFailException
from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from pendulum import datetime
from time import sleep
from datetime import timedelta



def my_long_running_func():
    sleep(10)
    return


default_args = {
    "owner": "was",
}

with DAG("lecture_4_timeout",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False,
         dagrun_timeout=timedelta(seconds=5),
         ) as dag:

    timeout = PythonOperator(
        task_id="my_long_running_func",
        python_callable=my_long_running_func,
        # execution_timeout=timedelta(seconds=3)
    )
