# Управление безопасностью

1. Устанавливаем Fernet key

    1.1. Генерируем ключ с помощью скрипта

    ```python
    from cryptography.fernet import Fernet

    fernet_key = Fernet.generate_key()
    print(fernet_key.decode())  # your fernet_key, keep it in secured place!
    ```

    1.2 Заполняем параметр конфигурации *fernet_key* сгенерированным ключем (my_fernet_key)

    ```ini
    fernet_key = my_fernet_key
    ```

    1.3 Перезапускаем Airflow, создаем переменную и убеждаемся что аттрибут *Is Encrypted  = True*

2. Ротируем Fernet key

    2.1 Генерируем новый Fernet key (new_fernet_key)

    2.2 Устанавливаем параметр конфигурацию *fernet_key* на *new_fernet_key,my_fernet_key*
    ```ini
    fernet_key = new_fernet_key,my_fernet_key
    ```

    2.3 Выполняем команду ротации с помощью консольной команды `airflow rotate-fernet-key`

    ```bash
    aifrlow rotate-fernet-key
    ```

    2.4 Устанавливаем параметр конфигурацию *fernet_key* на *new_fernet_key*
    
    ```ini
    fernet_key = new_fernet_key
    ```

    2.5 Перезапускаем Airflow для активации нового ключа
