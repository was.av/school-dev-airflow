import logging
from functools import partial

from airflow import DAG
from airflow.models import TaskInstance
from airflow.exceptions import AirflowFailException
from airflow.operators.python import PythonOperator
from airflow.operators.dummy import DummyOperator
from pendulum import datetime
from datetime import timedelta

from notification import task_retry_telegram_alert


def my_fail_func(ti: TaskInstance):
    logging.info(f"Current try_number: {ti.try_number}")
    if ti.try_number < 3:
        raise Exception("My Default Error")
    return


default_args = {
    "owner": "was",
    'depends_on_past': False,
    # retry notification
}

with DAG("lecture_4_retry",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False,
         ) as dag:
    fail = PythonOperator(
        task_id="my_fail_func",
        python_callable=my_fail_func,
        # retry
        retries=3,
        retry_delay=timedelta(seconds=3),
        # advanced retry
        retry_exponential_backoff=True,
        max_retry_delay=timedelta(minutes=3),
    )
    finish = DummyOperator(task_id="finish")
