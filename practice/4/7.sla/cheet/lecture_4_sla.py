from datetime import timedelta
from functools import partial

from airflow import DAG
from airflow.operators.bash import BashOperator
from pendulum import datetime

from notification import task_sla_missed_telegram_alert, task_fail_telegram_alert

default_args = {
    "owner": "was",
}

with DAG("lecture_4_sla",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval="*/2 * * * *",
         catchup=False,
         sla_miss_callback=partial(
             task_sla_missed_telegram_alert, user="was.av"),
         ) as dag:
    for i in range(1):
        BashOperator(
            task_id=f"sleep_10_{i}",
            bash_command="sleep 10",
            sla=timedelta(seconds=5),
            retries=0,
        )
