from typing import Any, Dict, List, Optional, Tuple

from airflow.models import DAG, TaskInstance
from airflow.exceptions import AirflowTaskTimeout
from airflow.providers.telegram.hooks.telegram import TelegramHook

TELEGRAM_CONN = 'telegram_default'


def get_users(user: str = None):
    default = "@was.av"
    return default if user is None else f"@{user}"


def send_telegram_alert(message: str, context: Optional[Dict[str, Any]] = None) -> None:
    hook = TelegramHook(telegram_conn_id=TELEGRAM_CONN)
    hook.send_message({"text": message})


def task_fail_telegram_alert(context, user: str = None):
    ti = context.get('task_instance')
    task_owner = context.get('task').owner
    if user is None and task_owner:
        user = task_owner
    
    exception = context.get('exception')
    error_type = 'Timeout' if isinstance(exception, AirflowTaskTimeout) else 'Failure'
    error_emoji = '⏱' if isinstance(exception, AirflowTaskTimeout) else '☠️'

    msg = f"""
        {error_emoji}  <strong>Task {error_type}:</strong> {get_users(user)}
        <b>Task</b>: {ti.task_id}
        <b>Dag</b>: {ti.dag_id}
        <b>Execution Time</b>: {context.get('logical_date')}
        <b>Log Url</b>: <a href="{ti.log_url}">{ti.log_url}</a> 
    """

    send_telegram_alert(message=msg)


def task_retry_telegram_alert(context, user: str = None):
    ti: TaskInstance = context.get('task_instance')
    task_owner = context.get('task').owner
    if user is None and task_owner:
        user = task_owner

    error_type =  f"Retry #{ti.try_number}/{ti.max_tries}"
    error_emoji = '🔁'

    msg = f"""
        {error_emoji}  <strong>Task {error_type}:</strong> {get_users(user)}
        <b>Task</b>: {ti.task_id}
        <b>Dag</b>: {ti.dag_id}
        <b>Execution Time</b>: {context.get('logical_date')}
        <b>Log Url</b>: <a href="{ti.log_url}">{ti.log_url}</a> 
    """

    send_telegram_alert(message=msg)


def task_success_telegram_alert(context, user: str = None) -> None:
    ti = context.get('task_instance')
    task_owner = context.get('task').owner
    if user is None and task_owner:
        user = task_owner
    # :skull_and_crossbones:
    msg = f"""
        🙀  <strong>Task Success:</strong> {get_users(user)}
        <b>Task</b>: {ti.task_id}
        <b>Dag</b>: {ti.dag_id}
        <b>Execution Time</b>: {context.get('logical_date')}
        <b>Log Url</b>: <a href="{ti.log_url}">{ti.log_url}</a> 
    """

    send_telegram_alert(message=msg)


def task_sla_missed_telegram_alert(dag: DAG,
                                   task_list: str,
                                   blocking_task_list: str,
                                   slas: List[Tuple],
                                   blocking_tis: List[TaskInstance],
                                   user: str) -> None:
    msg = f"""
        💥  <strong>Task SLA missed:</strong> {get_users(user)}
        <b>Dag</b>: {dag.dag_id}
        <b>Task(s)</b>: {task_list}
    """

    send_telegram_alert(message=msg)
