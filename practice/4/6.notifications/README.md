# Нотификации


1. Нотификации при падении в Telegram

    1.1 Установка провайдера Telegram

    ```bash
    pip install 'apache-airflow-providers-telegram' --constraint 'https://raw.githubusercontent.com/apache/airflow/constraints-2.2.2/constraints-3.8.txt'
    ```

    1.2 Создаем телеграмм бота и получаем токен (telegram_token)

    ```http
    https://core.telegram.org/bots#3-how-do-i-create-a-bot
    ```

    1.3 Создаем группу и добавляем туда нашего бота

    1.4 Пишем в группу и определяем ид группового чата

    ```http
    https://api.telegram.org/bot{{telegram_token}}/getUpdates
    ```

    1.5 Создаем Connection telegram_default и заполняем полученными данными

        Connection Id: telegram_default
        Connection Type: HTTP
        Password: telegram_token
        Registry URL: chat_id

    1.6 Копируем файл permissions.py в папку с дагами

    1.7 Создаем наш будущий DAG

    ```bash
    touch dags/lecture_4_notification.py
    ```

    1.8 Импортируем необходимые зависимости

    ```python
    from functools import partial

    from airflow import DAG
    from airflow.exceptions import AirflowFailException
    from airflow.operators.python import PythonOperator
    from pendulum import datetime

    from notification import task_fail_telegram_alert
    ```

    1.9 Описываем параметр по умолчанию и в качестве параметра user указываем свой логин

    ```python
    default_args = {
        "owner": "was",
        'on_failure_callback': partial(task_fail_telegram_alert, user="was.av"),
    }
    ```

    1.10 создаем даг c оператором который завершится ошибкой

    ```python
    def my_fail_func():
        raise AirflowFailException("My Error")

    with DAG("lecture_4_notification",
            default_args=default_args,
            start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
            schedule_interval=None,
            catchup=False,
            ) as dag:
        fail = PythonOperator(task_id="fail_func", python_callable=my_fail_func)
    ```

    1.11 Запускаем DAG, который завершится ошибкой и проверяем сообщение в телеграмм канале
