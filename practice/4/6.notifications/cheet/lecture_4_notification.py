from functools import partial

from airflow import DAG
from airflow.exceptions import AirflowFailException
from airflow.operators.python import PythonOperator
from pendulum import datetime

from notification import task_fail_telegram_alert, task_success_telegram_alert, task_sla_missed_telegram_alert


def my_fail_func():
    raise AirflowFailException("My Error")


default_args = {
    "owner": "was",
    'on_failure_callback': partial(task_fail_telegram_alert, user="was.av"),
    'on_success_callback': partial(task_success_telegram_alert, user="was.av"),
}

with DAG("lecture_4_notification",
         default_args=default_args,
         start_date=datetime(2021, 11, 11, tz="Europe/Kiev"),
         schedule_interval=None,
         catchup=False,
         ) as dag:
    fail = PythonOperator(task_id="fail_func", python_callable=my_fail_func)
