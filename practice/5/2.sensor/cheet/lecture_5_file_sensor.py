import airflow.utils.dates
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.sensors.filesystem import FileSensor

dag = DAG(
    dag_id="lecture_5_file_sensor",
    start_date=airflow.utils.dates.days_ago(3),
    schedule_interval="0 16 * * *",
    tags=["lecture_5"]
)

create_metrics = DummyOperator(task_id="create_metrics", dag=dag)

for id in range(1,3):
    wait = FileSensor(
        fs_conn_id='fs_default',
        task_id=f"wait_for_data_{id}",
        filepath=f"/workspaces/school-dev-airflow/dags/data/data_{id}.csv",
        poke_interval=10,
        timeout=60,
        dag=dag,
    )
    copy = DummyOperator(task_id=f"copy_to_raw_{id}", dag=dag)
    process = DummyOperator(task_id=f"process_data_{id}", dag=dag)
    wait >> copy >> process >> create_metrics

# touch dags/data/data_{1,2,3}.csv
# rm dags/data/data_{1,2,3}.csv