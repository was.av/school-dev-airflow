import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.google.cloud.operators.bigquery import BigQueryValueCheckOperator
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import \
    GCSToBigQueryOperator
from airflow.providers.google.cloud.transfers.postgres_to_gcs import PostgresToGCSOperator

bucket_name = "myairflowbucket"
gcs_filename = "rates/pg/{{ds_nodash}}.tsv.gz"
project_id = "myairflow-333619"
dataset_id = "myairflow"
table_name = "nbu_kurs"
table_schema = [
    {"name": "currency_id", "type": "INT64",
        "mode": "REQUIRED", "description": "ID валюты"},
    {"name": "currency_name", "type": "STRING",
     "mode": "REQUIRED", "description": "Название валюты"},
    {"name": "rate", "type": "FLOAT", "mode": "REQUIRED",
        "description": "Курс относительно ГРН"},
    {"name": "currency_code", "type": "STRING",
        "mode": "REQUIRED", "description": "Код валюты"},
    {"name": "date", "type": "DATE", "mode": "REQUIRED", "description": "Дата"},
]

default_args = {
    "owner": "was",
    "project_id": project_id,
    # "depends_on_past": True,
}
with DAG(
    'lecture_2_pg2bg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False,
    default_args=default_args
) as dag:
    
    pg2gcs = PostgresToGCSOperator(
        task_id="pg2gcs",
        postgres_conn_id="postgres_airflow",
        sql="""\
            select currency_name,currency_code,rate, date 
            from public.nbu_kurs 
            where date between SYMMETRIC '{{ ds }}' and '{{ macros.ds_add(ds, -1) }}'
            """,
        export_format="csv",
        field_delimiter="\t",
        gcp_conn_id="gcp_airflow",
        bucket=bucket_name,
        filename=gcs_filename,
        gzip=True,
        use_server_side_cursor=True
    )

    upload_to_bigquery = GCSToBigQueryOperator(
        task_id="import_in_bigquery",
        google_cloud_storage_conn_id="gcs_airflow",
        bucket=bucket_name,
        source_objects=[gcs_filename],
        source_format="CSV",                                # формат файла
        field_delimiter="\t",                               # разделить файла
        compression="GZIP",                                 # файл сжат алгоритмом gzip
        skip_leading_rows=1,                                # пропускаем заголовки
        schema_fields=table_schema,
        bigquery_conn_id="gcp_airflow",
        destination_project_dataset_table=f"{project_id}:{dataset_id}.{table_name}" + \
        "_{{ds_nodash}}",
        # создать таблицу, если она отсутвует https://cloud.google.com/bigquery/docs/reference/rest/v2/Job
        create_disposition="CREATE_IF_NEEDED",
        # перед загрузкой данных очистить таблицу
        write_disposition="WRITE_TRUNCATE",
    )

    check_has_records = BigQueryValueCheckOperator(
        task_id="check_has_records",
        gcp_conn_id="gcp_airflow",
        sql=f"select count(currency_id) from {dataset_id}.{table_name}" +
        "_{{ds_nodash}}",
        pass_value=60,
    )

    pg2gcs >> upload_to_bigquery >> check_has_records
