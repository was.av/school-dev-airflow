# Google Provider API2BG


1. Создадим подключение для работы с Google Cloud provider используя WEB интерфейс

2. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_2_api2bg.py
```

3. Импортируем необходимые библиотеки, операторы и хуки

```python
import logging

import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from airflow.providers.google.cloud.operators.bigquery import \
    BigQueryCreateEmptyTableOperator, BigQueryValueCheckOperator
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import \
    GCSToBigQueryOperator
```

4. Добалвяем переменные с gcs бакетом, адресными данными таблицы BigQuery (проект, датасет, таблица) и схемой таблицы

```python
bucket_name = "myairflowbucket"
project_id = "myairflow-333619"
dataset_id = "myairflow"
table_name = "nbu_kurs"

table_schema = [
    {"name": "currency_id", "type": "INT64",
        "mode": "REQUIRED", "description": "ID валюты"},
    {"name": "currency_name", "type": "STRING",
     "mode": "REQUIRED", "description": "Название валюты"},
    {"name": "rate", "type": "FLOAT", "mode": "REQUIRED",
        "description": "Курс относительно ГРН"},
    {"name": "currency_code", "type": "STRING",
        "mode": "REQUIRED", "description": "Код валюты"},
    {"name": "date", "type": "DATE", "mode": "REQUIRED", "description": "Дата"},
]
```

5. Создаем DAG c ежневным выполнением 

```python
default_args = {
    "owner": "was",
    "project_id": project_id,
    # "depends_on_past": True,
}
with DAG(
    'lecture_2_api2bg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False,
    default_args=default_args
) as dag:
    ...
```

6. Созадим функцию по скачиванию данных о курсах валют и загрузки на GCS:

```python
def download_nbu_rates_to_gcs(ds_nodash, ti):
    import json
    import os
    import tempfile
    from os import path

    import pandas as pd
    import requests

    # fetch data
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")
    data = json.loads(response.text)
    # transform
    df = pd.DataFrame(data)
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    # upload to gcs
    gcs_hook = GCSHook(google_cloud_storage_conn_id="gcp_airflow")
    object_name = f"rates/nbu/{ds_nodash}.tsv.gz"

    gcs_hook.upload(bucket_name=bucket_name,
                    object_name=object_name,
                    data=df.to_csv(sep="\t", index=False),
                    gzip=True,
                    )

    ti.xcom_push(key="object_name", value=object_name)
```

7. Добавим оператор PythonOperator для запуска функции *download_nbu_rates_to_gcs*

```python
download_nbu_rates_to_gcs_op = PythonOperator(
        task_id="download_nbu_rates_to_gcs",
        python_callable=download_nbu_rates_to_gcs,
    )
```

8. Запускаем даг и проверяем наличи файла в бакете

```
airflow dags test lecture_2_api2bg 2021-10-11
```

9. Проблуем повторно запустить DAG и получаем ошибку о том что такой файл уже существует - задача не являентся идемпотентной.
Добавляем удаление файла в бакете, если он существует в функцию *download_nbu_rates_to_gcs* перед загурзкой файла в бакет

```python

if gcs_hook.exists(bucket_name, object_name):
    logging.warning(
        f"Rates file gs://{bucket_name}/{object_name} is altedy exists.")
    gcs_hook.delete(bucket_name, object_name)
    logging.warning(
        f"Rates file gs://{bucket_name}/{object_name} is deleted.")
```

10. Запускаем даг и проверяем что даг успешно отработал

```sh
airflow dags test lecture_2_api2bg 2021-10-11
```

11. Доабвляем оператор для загрузки данных в Bucket

```python 
    upload_to_bigquery = GCSToBigQueryOperator(
        task_id="import_in_bigquery",
        google_cloud_storage_conn_id="gcp_airflow",
        bucket=bucket_name,
        source_objects=[
            "{{ ti.xcom_pull(task_ids='download_nbu_rates_to_gcs_op', key='object_name') }}",
        ],
        source_format="CSV",                                # формат файла
        field_delimiter="\t",                               # разделить файла
        compression="GZIP",                                 # файл сжат алгоритмом gzip
        skip_leading_rows=1,                                # пропускаем заголовки
        schema_fields=table_schema,
        bigquery_conn_id="gcp_airflow",
        destination_project_dataset_table=f"{project_id}:{dataset_id}.{table_name}" + \
        "_{{ds_nodash}}",
        # создать таблицу, если она отсутвует https://cloud.google.com/bigquery/docs/reference/rest/v2/Job
        create_disposition="CREATE_IF_NEEDED",
        # перед загрузкой данных очистить таблицу
        write_disposition="WRITE_TRUNCATE",
    )
```

12. Настраиваем цепочку запуска задач

```python
download_nbu_rates_to_gcs_op >> upload_to_bigquery 
```

13. Запускаем даг и проверяем что данные загружены в bigquery

```sh
airflow dags test lecture_2_api2bg 2021-10-11
```

14. Добавляем оператор проверки кол-ва данных в БД

```python
check_has_records = BigQueryValueCheckOperator(
        task_id="check_has_records",
        gcp_conn_id="gcp_airflow",
        sql=f"select count(currency_id) from {dataset_id}.{table_name}" +
        "_{{ds_nodash}}",
        pass_value=60,
    )
```

15. Модифицируем цепочку выполнения DAG

```python
download_nbu_rates_to_gcs_op >> upload_to_bigquery >> check_has_records
```

16. Запускаем даг и проверяем что оператор проверки успешно выполнен. Можно сменить значение 60 на другое и задача завершится с ошибкой
```sh
airflow dags test lecture_2_api2bg 2021-10-11
```