import logging

import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from airflow.providers.google.cloud.operators.bigquery import \
    BigQueryValueCheckOperator
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import \
    GCSToBigQueryOperator

bucket_name = "myairflowbucket"
project_id = "myairflow-333619"
dataset_id = "myairflow"
table_name = "nbu_kurs"

table_schema = [
    {"name": "currency_id", "type": "INT64",
        "mode": "REQUIRED", "description": "ID валюты"},
    {"name": "currency_name", "type": "STRING",
     "mode": "REQUIRED", "description": "Название валюты"},
    {"name": "rate", "type": "FLOAT", "mode": "REQUIRED",
        "description": "Курс относительно ГРН"},
    {"name": "currency_code", "type": "STRING",
        "mode": "REQUIRED", "description": "Код валюты"},
    {"name": "date", "type": "DATE", "mode": "REQUIRED", "description": "Дата"},
]


def download_nbu_rates_to_gcs(ds_nodash, ti):
    import json
    import os
    import tempfile
    from os import path
    import logging

    import pandas as pd
    import requests

    # fetch data
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")
    data = json.loads(response.text)
    # transform
    df = pd.DataFrame(data)
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)
    # upload to gcs
    gcs_hook = GCSHook(google_cloud_storage_conn_id="gcp_airflow")
    object_name = f"rates/nbu/{ds_nodash}.tsv.gz"
    if gcs_hook.exists(bucket_name, object_name):
        logging.warning(
            f"Rates file gs://{bucket_name}/{object_name} is altedy exists.")
        gcs_hook.delete(bucket_name, object_name)
        logging.info(
            f"Rates file gs://{bucket_name}/{object_name} is deleted.")

    gcs_hook.upload(bucket_name=bucket_name,
                    object_name=object_name,
                    data=df.to_csv(sep="\t", index=False),
                    gzip=True,
                    )

    ti.xcom_push(key="object_name", value=object_name)


default_args = {
    "owner": "was",
    "project_id": project_id,
    # "depends_on_past": True,
}
with DAG(
    'lecture_2_api2bg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False,
    default_args=default_args
) as dag:
    download_nbu_rates_to_gcs_op = PythonOperator(
        task_id="download_nbu_rates_to_gcs",
        python_callable=download_nbu_rates_to_gcs,
    )

    upload_to_bigquery = GCSToBigQueryOperator(
        task_id="import_in_bigquery",
        google_cloud_storage_conn_id="gcp_airflow",
        bucket=bucket_name,
        source_objects=[
            "{{ ti.xcom_pull(task_ids='download_nbu_rates_to_gcs', key='object_name') }}",
        ],
        source_format="CSV",                                # формат файла
        field_delimiter="\t",                               # разделить файла
        compression="GZIP",                                 # файл сжат алгоритмом gzip
        skip_leading_rows=1,                                # пропускаем заголовки
        schema_fields=table_schema,
        bigquery_conn_id="gcp_airflow",
        destination_project_dataset_table=f"{project_id}:{dataset_id}.{table_name}" + \
        "_{{ds_nodash}}",
        # создать таблицу, если она отсутвует https://cloud.google.com/bigquery/docs/reference/rest/v2/Job
        create_disposition="CREATE_IF_NEEDED",
        # перед загрузкой данных очистить таблицу
        write_disposition="WRITE_TRUNCATE",
    )

    # check_has_records = BigQueryValueCheckOperator(
    #     task_id="check_has_records",
    #     gcp_conn_id="gcp_airflow",
    #     sql=f"select count(currency_id) from {dataset_id}.{table_name}" +
    #     "_{{ds_nodash}}",
    #     pass_value=60,
    # )

    # download_nbu_rates_to_gcs_op >> upload_to_bigquery >> check_has_records
