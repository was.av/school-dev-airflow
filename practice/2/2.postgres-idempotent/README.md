# Postgre Provider


1. Создаем файл с названием "lecture_2_api2pg_idemp.py" для ногового  дага

```bash
touch dags/lecture_2_api2pg_idemp.py
```

2. Импортируем необхоиые библиотеки, операторы и хук

```python
import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator
```


3. Добалвяем переменные с названием подключения и таблицы

```python
table_name = "nbu_kurs"
pg_conn_id = "postgres_airflow"
```

4. Создаем DAG c ежневным выполнением

```python
with DAG(
    'lecture_2_api2pg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False
) as dag:
    ...
```

5. Создаем файл sql файл с запросом с названием *dags/lecture_2_api2pg_idemp_create_table.sql* со следующим содержимым:

```sql
CREATE TABLE if NOT EXISTS {{ params.table_name}} (
    currency_id INT,
    currency_name VARCHAR(100),
    currency_code CHAR(3),
    rate FLOAT,
    "date" DATE,
    primary key(
        currency_id,
        DATE
    )
);
CREATE TABLE if NOT EXISTS _{{ params.table_name }} (
    currency_id INT,
    currency_name VARCHAR(100),
    currency_code CHAR(3),
    rate FLOAT,
    "date" DATE,
    primary key(
        currency_id,
        DATE
    )
);

```

6. Добалвяем оператор PostgresOperator с запросом на создание таблицы на основе файла *dags/lecture_2_api2pg_idemp_create_table.sql*:

```python
create_table_op = PostgresOperator(
        task_id="create_table_op",
        postgres_conn_id=pg_conn_id,
        sql="lecture_2_api2pg_idemp_create_table.sql",
        params={"table_name": table_name}
    )
```

7. Создадим функцию с названием *download_nbu_rates* для скачивания и загрузки данных в Postgres c учетом идемпотентности задачи

```python
def download_nbu_rates(ds_nodash):
    import json

    import pandas as pd
    import requests

    # extract
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")

    # transform
    df = pd.DataFrame(json.loads(response.text))
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)

    # load
    pg_hook = PostgresHook(postgres_conn_id=pg_conn_id)
    pg_hook.run(f"truncate table _{table_name};")
    pg_hook.insert_rows(
        table=f"_{table_name}",
        rows=[list(row) for row in df.itertuples(index=False)],
        target_fields=list(df.keys()),
    )
    pg_hook.run(f"""\
        delete from {table_name} as t1 using _-{table_name} as t2
        where t1.currency_id = t2.currency_id and t1.date = t2.date;
        insert into {table_name} select * from _{table_name};
        truncate table _{table_name};
        """)
```

8. Доабвляем оператор PythonOperator для запуска функции *download_nbu_rates*

```python
download_nbu_rates_op = PythonOperator(
        task_id="download_nbu_rates_op",
        python_callable=download_nbu_rates,
    )
```

9. Описываем последовательность выполнения задач DAG

```python
create_table_op >> download_nbu_rates_op
```

10. Запускаем dag

```sh
airflow dags test lecture_2_api2pg_idemp 2021-10-11
```

