import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator


table_name = "nbu_kurs"
pg_conn_id = "postgres_airflow"

def download_nbu_rates(ds_nodash):
    import json

    import pandas as pd
    import requests

    # extract
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")

    # transform
    df = pd.DataFrame(json.loads(response.text))
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)

    # load
    pg_hook = PostgresHook(postgres_conn_id=pg_conn_id)
    pg_hook.run(f"truncate table _{table_name};")
    pg_hook.insert_rows(
        table=f"_{table_name}",
        rows=[list(row) for row in df.itertuples(index=False)],
        target_fields=list(df.keys()),
    )
    pg_hook.run(f"""\
        delete from {table_name} as t1 using _-{table_name} as t2
        where t1.currency_id = t2.currency_id and t1.date = t2.date;
        insert into {table_name} select * from _{table_name};
        truncate table _{table_name};
        """)


with DAG(
    'lecture_2_api2pg_idemp',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False
) as dag:
    create_table_op = PostgresOperator(
        task_id="create_table_op",
        postgres_conn_id=pg_conn_id,
        sql="dags/lecture_2_api2pg_idemp_create_table.sql",
        params={"table_name": table_name}
    )

    download_nbu_rates_op = PythonOperator(
        task_id="download_nbu_rates_op",
        python_callable=download_nbu_rates,
    )

    create_table_op >> download_nbu_rates_op
