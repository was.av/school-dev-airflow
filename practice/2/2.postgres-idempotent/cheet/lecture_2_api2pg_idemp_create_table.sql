CREATE TABLE if NOT EXISTS {{ params.table_name}} (
    currency_id INT,
    currency_name VARCHAR(100),
    currency_code CHAR(3),
    rate FLOAT,
    "date" DATE,
    primary key(
        currency_id,
        DATE
    )
);
CREATE TABLE if NOT EXISTS _{{ params.table_name }} (
    currency_id INT,
    currency_name VARCHAR(100),
    currency_code CHAR(3),
    rate FLOAT,
    "date" DATE,
    primary key(
        currency_id,
        DATE
    )
);
