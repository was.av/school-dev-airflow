# Postgre Provider


1. Создаем файл с названием "lecture_2_api2pg.py" для ногового  дага

```bash
touch dags/lecture_2_api2pg.py
```

2. Импортируем необхоиые библиотеки, операторы и хук

```python
import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator
```


3. Добалвяем переменные с названием подключения и таблицы

```python
table_name = "nbu_kurs"
pg_conn_id = "postgres_airflow"
```

4. Создаем DAG c ежневным выполнением

```python
with DAG(
    'lecture_2_api2pg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False
) as dag:
    ...
```

5. Добалвяем оператор PostgresOperator с запросом на создание таблицы

```python
create_table_op = PostgresOperator(
    task_id="create_table_op",
    postgres_conn_id=pg_conn_id,
    sql=f"""\
        create table if not exists {table_name} (
            currency_id int,
            currency_name varchar(100),
            currency_code char(3),
            rate float,
            date date,
            primary key(currency_id, date)
        );
        """
)
```

6. Запускаем даг и проверяем что таблицы создались

7. Создадим функцию с названием ** для скачивания и загрузки данных в Postgres

```python
def download_nbu_rates(ds_nodash):
    import json

    import pandas as pd
    import requests

    # extract
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")

    # transform
    df = pd.DataFrame(json.loads(response.text))
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)

    # load
    pg_hook = PostgresHook(postgres_conn_id=pg_conn_id)
    pg_hook.insert_rows(
        table=f"{table_name}",
        rows=[list(row) for row in df.itertuples(index=False)],
        target_fields=list(df.keys()),
    )
```

8. Доабвляем оператор PythonOperator для запуска функции *download_nbu_rates*

```python
download_nbu_rates_op = PythonOperator(
        task_id="download_nbu_rates_op",
        python_callable=download_nbu_rates,
    )
```

9. Описываем последовательность выполнения задач DAG

```python
create_table_op >> download_nbu_rates_op
```

10. Запускаем dag

```sh
airflow dags test lecture_2_api2pg 2021-10-11
```
