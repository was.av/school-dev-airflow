import pendulum
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator


table_name = "nbu_kurs"
pg_conn_id = "postgres_airflow"

def download_nbu_rates(ds_nodash):
    import json

    import pandas as pd
    import requests

    # extract
    response = requests.get(
        f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/?date={ds_nodash}&json")

    # transform
    df = pd.DataFrame(json.loads(response.text))
    df.rename(columns={
        'r030': 'currency_id',
        'cc': 'currency_code',
        'txt': 'currency_name',
        'exchangedate': 'date',
    }, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=True)

    # load
    pg_hook = PostgresHook(postgres_conn_id=pg_conn_id)
    pg_hook.insert_rows(
        table=f"{table_name}",
        rows=[list(row) for row in df.itertuples(index=False)],
        target_fields=list(df.keys()),
    )


with DAG(
    'lecture_2_api2pg',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False
) as dag:
    create_table_op = PostgresOperator(
        task_id="create_table_op",
        postgres_conn_id=pg_conn_id,
        sql=f"""\
            create table if not exists {table_name} (
                currency_id int,
                currency_name varchar(100),
                currency_code char(3),
                rate float,
                date date,
                primary key(currency_id, date)
            );
            """
    )


    download_nbu_rates_op = PythonOperator(
        task_id="download_nbu_rates_op",
        python_callable=download_nbu_rates,
    )

    create_table_op >> download_nbu_rates_op
