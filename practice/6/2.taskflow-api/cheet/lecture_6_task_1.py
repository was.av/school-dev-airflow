import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.decorators import dag, task


default_args = {
    "owner": "was"
}

@dag(default_args=default_args,
     schedule_interval="@daily",
     start_date=pendulum.datetime(2021, 10, 10, tz="Europe/Kiev"),
     catchup=False)
def lecture_6_task_1():

    @task(multiple_outputs=True)
    def extract_covid_data(**kwargs):
        ds = kwargs['ds']
        import requests
        response = requests.get(f"https://api-covid19.rnbo.gov.ua/data?to={ds}")
        response.raise_for_status()
        return {"covid": response.json()}

    @task
    def compare_covid_confirm(covid, **kwargs):
        ti = kwargs['ti']
        regions = Variable.get("covid_region", "Dnipropetrovska")
        confirmed_prev = Variable.get(
            "covid_region_confirmed_prev", 0)
        message = ""
        for region in covid["ukraine"]:
            if region["label"]["en"] in regions \
                    or region["label"]["uk"] in regions:
                confirmed = int(region["confirmed"])
                confirmed_key = f"covid_region_{region['label']['en']}_confirmed_prev"
                confirmed_prev = int(Variable.get(confirmed_key, 0))
                change = confirmed - confirmed_prev
                change = f"+{change}" if change > 0 else str(change)
                message += f"Подтвержденные случаи COVID в {region['label']['uk']} - {confirmed} ({change})\n"            
                Variable.set(confirmed_key, confirmed)

        return message.rstrip()


    send_message = BashOperator(
        task_id="send_message",
        bash_command="""\
            echo "{{ ti.xcom_pull(task_ids="compare_covid_confirm", key="return_value") }}"
            """
    )

    covid_data = extract_covid_data()
    compare_covid_confirm(covid_data['covid'])  >> send_message

dag = lecture_6_task_1()