import logging
import sys
from datetime import datetime
from typing import Dict

import numpy as np
from airflow.decorators import dag, task
from airflow.operators.dummy import DummyOperator


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)



@dag(start_date=datetime(2021, 11, 1), schedule_interval=None, catchup=False, tags=["lecture_6"])
def lecture_6_taskflow_api_complex():

    @task()
    def decorator_task() -> Dict[str, dict]:
        logging.info(f"EXTRACT: Python version: {sys.version}")
        logging.info(f"EXTRACT: Numpy version: {np.__version__}")
        return {"task": {"python_version": sys.version, "numpy_version": np.__version__}}

    @task.docker(image='python:3.9-slim-buster', auto_remove=True, multiple_outputs=True)
    def docker_task(data: dict):
        return dict(**data, **{"docker": {"python_version": sys.version}})

    @task.virtualenv(
        use_dill=True,
        system_site_packages=False,
        requirements=['numpy==1.21.4'],
        multiple_outputs=True
    )
    def venv_task(data):
        import logging
        import platform

        import numpy as np

        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
        logging.info(f"LOAD: Python version: {sys.version}")
        logging.info(f"LOAD: Numpy version: {np.__version__}")
        return dict(**data, **{"virtualenv": {"python_version": platform.python_version(), "numpy_version": np.__version__}})

    @task()
    def dump(data: dict):
        import json
        s = json.dumps(data, indent=2)
        logging.info(f"DUMP: {s}")


    end = DummyOperator(task_id="end")

    dump_task_result = dump(venv_task(docker_task(decorator_task())))
    dump_task_result >> end

# Required
dag = lecture_6_taskflow_api_complex()

