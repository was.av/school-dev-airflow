import logging
import sys
from datetime import datetime
from typing import Dict

import numpy as np
from airflow.decorators import dag, task
from airflow.operators.dummy import DummyOperator


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


@dag(start_date=datetime(2021, 11, 1), schedule_interval=None, catchup=False, tags=["lecture_6"])
def lecture_6_taskflow_api_docker():

    @task.docker(image='python:3.9-slim-buster', auto_remove=True, multiple_outputs=True)
    def docker_task():
        return {"docker": {"python_version": sys.version}}

    @task()
    def dump(data: dict):
        import json
        s = json.dumps(data, indent=2)
        logging.info(f"DUMP: {s}")

    data = docker_task()
    dump(data)

# Required
dag = lecture_6_taskflow_api_docker()
