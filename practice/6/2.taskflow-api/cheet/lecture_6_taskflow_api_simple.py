import logging
import sys
from datetime import datetime
from typing import Dict

import numpy as np
from airflow.decorators import dag, task


@dag(start_date=datetime(2021, 11, 1), schedule_interval=None, catchup=False, tags=["lecture_6"])
def lecture_6_taskflow_api_simple():

    @task()
    def decorator_task() -> Dict[str, dict]:
        logging.info(f"EXTRACT: Python version: {sys.version}")
        logging.info(f"EXTRACT: Numpy version: {np.__version__}")
        return {"task": {"python_version": sys.version, "numpy_version": np.__version__}}

    decorator_task()

dag = lecture_6_taskflow_api_simple()
