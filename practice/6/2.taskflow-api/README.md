# Taskflow API

Для повышения удобства и четаемости кода в Airflow 2 повился альтернативный способо описания DAG-ов с помощью декораторов.

Реализуем задачу из первой лекции используя декораторы. Реализацию данной задачи можно найти [practice/1/3.practice/cheet/lecture_1_task_2.py](../../1/3.practice/cheet/lecture_1_task_2.py)

1. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_6/lecture_6_task_1.py
```

2. Импортируем необходимые библиотеки, операторы и хуки

```python
import pendulum
from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash import BashOperator
from airflow.decorators import dag, task
```

3. Создаем DAG c ежневным выполнением используя функцию и декоратор @dag

```python
@dag(default_args=default_args,
     schedule_interval="@daily",
     start_date=pendulum.datetime(2021, 10, 10, tz="Europe/Kiev"),
     catchup=False)
def lecture_6_task_1():
```


4. Добавляем фунцию загрузки данных внутри функции дага и помечаем её декоратором. Т.к. ответ будет состоять из JSON, то добавляем аттрибут декоратора multiple_outputs=True 

```python
@task(multiple_outputs=True)
def extract_covid_data(**kwargs):
    ds = kwargs['ds']
    import requests
    response = requests.get(f"https://api-covid19.rnbo.gov.ua/data?to={ds}")
    response.raise_for_status()
    return {"covid": response.json()}
```

5.  Добавляем фунцию сравнения данных внутри функции дага и помечаем её декоратором. 


```python
@task
def compare_covid_confirm(covid, **kwargs):
    ti = kwargs['ti']
    regions = Variable.get("covid_region", "Dnipropetrovska")
    confirmed_prev = Variable.get(
        "covid_region_confirmed_prev", 0)
    message = ""
    for region in covid["ukraine"]:
        if region["label"]["en"] in regions \
                or region["label"]["uk"] in regions:
            confirmed = int(region["confirmed"])
            confirmed_key = f"covid_region_{region['label']['en']}_confirmed_prev"
            confirmed_prev = int(Variable.get(confirmed_key, 0))
            change = confirmed - confirmed_prev
            change = f"+{change}" if change > 0 else str(change)
            message += f"Подтвержденные случаи COVID в {region['label']['uk']} - {confirmed} ({change})\n"            
            Variable.set(confirmed_key, confirmed)

    return message.rstrip()
```

6. Переиспользуем задачу с печатью в консоль `send_message` в рамках функции дага

```python
send_message = BashOperator(
    task_id="send_message",
    bash_command="""\
        echo "{{ ti.xcom_pull(task_ids="compare_covid_confirm", key="return_value") }}"
        """
)
```

7. Связываем задачи между собой в рамках функции дага. При вызове задачи сравнения данных указываем какие именно данные необходимо передать.

```python
covid_data = extract_covid_data()
compare_covid_confirm(covid_data['covid'])  >> send_message
```

8. Создаем DAG

```python
dag = lecture_6_task_1()
```

9. Запускаем даг
```sh
airflow dags test lecture_6_task_1 2022-01-01
```