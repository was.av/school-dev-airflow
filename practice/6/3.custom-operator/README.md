# Custom Hook и Operator

Для повышения удобства и четаемости кода в Airflow 2 повился альтернативный способо описания DAG-ов с помощью декораторов.

Реализуем задачу из второй лекции используя кастомный оператор и хук. Реализацию данной задачи можно найти [practice/2/3.api2bq/cheet/lecture_2_api2bg.py](../../2/3.api2bq/cheet/lecture_2_api2bg.py)


1. Создадим подключение для работы с Google Cloud provider используя WEB интерфейс

2. Создадим стркутуру нашего провайдера и файл кастомного хука hooks/bank.py
```sh
mkdir dags/common
touch dags/common/__init.py
mkdir dags/common/govua/
touch dags/common/govua/__init.py
mkdir dags/common/govua/hooks/
touch dags/common/govua/hooks/__init.py
touch dags/common/govua/hooks/bank.py
```

3. Создаем класс ExchangeHook и на следуемся от BaseHook. Реализуем единственным метод для оплучения данных из API

```python
import requests
from airflow.hooks.base import BaseHook

class ExchangeHook(BaseHook):

    def get_records(self, date_to):
        response = requests.get("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/",
                                params={"date": date_to, "json": 1},
                                headers={"Content-Type": "application/json"},
                                )
        response.raise_for_status()
        return response.json()

```

4. Создадим файл кастомного jgthfnjhf operators/bank.pyoperators

```sh
mkdir dags/common/govua/operators/
touch dags/common/govua/operators/__init.py
touch dags/common/govua/operators/bank.py
```

5. Создаем свой оператор на `LoadExachangeToGCSTsvOperator` для загрузи данных из API Covid-19 в Google Cloud Storage переиспользуя код их решенной задачи второй лекции. Новый орпетор должен принять все необходимые параметры и реализовать метод `execute`

```python
import json
from datetime import date

from typing import Any, Dict

import pandas as pd
import requests
from airflow.models import BaseOperator
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from common.govua.hooks.bank import ExchangeHook


class LoadExachangeToGCSTsvOperator(BaseOperator):

    template_fields = ['_date_to', '_bucket_name', '_object_name']

    ui_color = "#337ab7" # blue
    ui_fgcolor = "#e5f9c6" # green

    def __init__(
            self,
            date_to: str,
            gcp_conn_id: str,
            bucket_name: str,
            object_name: str,
            gzip: bool = False,
            *args,
            **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.date_to = date_to
        self.gcp_conn_id = gcp_conn_id
        self.bucket_name = bucket_name
        self.object_name = object_name
        self.gzip = gzip

    def execute(self, context: Dict[str, Any]) -> Any:
        exchange_hook = ExchangeHook()
        gcs_hook = GCSHook(gcp_conn_id=self.gcp_conn_id)

        records = exchange_hook.get_records(self.date_to)
        df = pd.DataFrame(records)
        df = self._transform(df)
        gcs_hook.upload(bucket_name=self.bucket_name,
                        object_name=self.object_name,
                        data=df.to_csv(sep="\t", index=False),
                        gzip=self.gzip,
                        )
        return

    def _transform(self, df):
        df.rename(columns={
            'r030': 'currency_id',
            'cc': 'currency_code',
            'txt': 'currency_name',
            'exchangedate': 'date',
        }, inplace=True)
        df['date'] = pd.to_datetime(df['date'], dayfirst=True)
        return df

```

6. Создадим файл с нашим будущим дагом:

```sh
touch dags/lecture_6/lecture_6_task_2.py
```

7. Импортируем необходимые библиотеки, операторы и хуки включая свой оператор

```python
from datetime import date

import pendulum
from airflow import DAG
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import \
    GCSToBigQueryOperator
from common.govua.operators.bank import LoadExachangeToGCSTsvOperator
```


8.  Добалвяем переменные с gcs бакетом, адресными данными таблицы BigQuery (проект, датасет, таблица) и схемой таблицы

```python
gcp_conn_id = "УКАЗЫВАЕМ СОБСТВЕННЫЙ ИДЕНТФИКАТОР ПОДКЛЮЧЕНИЯ"
bucket_name = "УКАЗЫВАЕМ СОБСТВЕННЫЙ БАКЕТ" # должен быть заранее создан

default_args = {
    "owner": "was",
    "gcp_conn_id": gcp_conn_id,
    "bucket_name": bucket_name,
}

dataset_id = "УКАЗЫВАЕМ СОБСТВЕННЫЙ ДАТАСЕТ в BIGQUERY" # должен быть заранее создан
table_name = "exchange_nbu_v2"
table_schema = [
    {"name": "currency_id", "type": "INT64",
        "mode": "REQUIRED", "description": "ID валюты"},
    {"name": "currency_name", "type": "STRING",
     "mode": "REQUIRED", "description": "Название валюты"},
    {"name": "rate", "type": "FLOAT", "mode": "REQUIRED",
        "description": "Курс относительно ГРН"},
    {"name": "currency_code", "type": "STRING",
        "mode": "REQUIRED", "description": "Код валюты"},
    {"name": "date", "type": "DATE", "mode": "REQUIRED", "description": "Дата"},
]
```

9. Создаем DAG c ежневным выполнением используя функцию и декоратор @dag

```python
with DAG(
    'lecture_6_task_2',
    start_date=pendulum.datetime(2021, 11, 11, tz="Europe/Kiev"),
    schedule_interval="@daily",
    catchup=False,
    default_args=default_args
) as dag:
    ...
```


10. Добавляем задачу выгрузки данных по курсам на GSC используя собственный оператор

```python
load_exchange_data_to_gcs_custom_operator = LoadExachangeToGCSTsvOperator(
        task_id="load_exchange_data_to_gcs_custom_operator",
        date_to="{{ds_nodash}}",
        bucket_name=bucket_name,
        object_name="rates/nbu_2/{{ds_nodash}}.tsv.gz",
        gzip=True
    )
```

11.  Переиспользуем код импорта данных из GCS 

```python
upload_to_bigquery = GCSToBigQueryOperator(
        task_id="upload_to_bigquery",
        bigquery_conn_id=gcp_conn_id,
        google_cloud_storage_conn_id=gcp_conn_id,
        bucket=bucket_name,
        source_objects=["rates/nbu_2/{{ds_nodash}}.tsv.gz"],
        source_format="CSV",                                # формат файла
        field_delimiter="\t",                               # разделить файла
        compression="GZIP",                                 # файл сжат алгоритмом gzip
        skip_leading_rows=1,                                # пропускаем заголовки
        schema_fields=table_schema,
        destination_project_dataset_table=f"{dataset_id}.{table_name}" + \
        "_{{ds_nodash}}",
        # создать таблицу, если она отсутвует https://cloud.google.com/bigquery/docs/reference/rest/v2/Job
        create_disposition="CREATE_IF_NEEDED",
        # перед загрузкой данных очистить таблицу
        write_disposition="WRITE_TRUNCATE",
    )
```

12. Связываем цепуска задач DAG-a

```python
load_exchange_data_to_gcs_custom_operator >> upload_to_bigquery
```

13. Запускаем даг.
```sh
airflow dags test lecture_6_task_2 2022-01-01
```