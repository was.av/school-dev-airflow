import json
from datetime import date

from typing import Any, Dict

import pandas as pd
import requests
from airflow.models import BaseOperator
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from common.govua.hooks.bank import ExchangeHook


class LoadExachangeToGCSTsvOperator(BaseOperator):

    template_fields = ['_date_to', '_bucket_name', '_object_name']

    ui_color = "#337ab7" # blue
    ui_fgcolor = "#e5f9c6" # green

    def __init__(
            self,
            date_to: str,
            gcp_conn_id: str,
            bucket_name: str,
            object_name: str,
            gzip: bool = False,
            *args,
            **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.date_to = date_to
        self.gcp_conn_id = gcp_conn_id
        self.bucket_name = bucket_name
        self.object_name = object_name
        self.gzip = gzip

    def execute(self, context: Dict[str, Any]) -> Any:
        exchange_hook = ExchangeHook()
        gcs_hook = GCSHook(gcp_conn_id=self.gcp_conn_id)

        records = exchange_hook.get_records(self.date_to)
        df = pd.DataFrame(records)
        df = self._transform(df)
        gcs_hook.upload(bucket_name=self.bucket_name,
                        object_name=self.object_name,
                        data=df.to_csv(sep="\t", index=False),
                        gzip=self.gzip,
                        )
        return

    def _transform(self, df):
        df.rename(columns={
            'r030': 'currency_id',
            'cc': 'currency_code',
            'txt': 'currency_name',
            'exchangedate': 'date',
        }, inplace=True)
        df['date'] = pd.to_datetime(df['date'], dayfirst=True)
        return df
