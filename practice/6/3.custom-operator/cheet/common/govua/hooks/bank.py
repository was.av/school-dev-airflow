import requests
from airflow.hooks.base import BaseHook



class ExchangeHook(BaseHook):

    def get_records(self, date_to):
        response = requests.get("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange/",
                                params={"date": date_to, "json": 1},
                                headers={"Content-Type": "application/json"},
                                )
        response.raise_for_status()
        return response.json()
