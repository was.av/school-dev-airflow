"""
Redesign lecture 2 api2bg
"""
from datetime import date

import pendulum
from airflow import DAG
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import \
    GCSToBigQueryOperator
from common.govua.operators.bank import LoadExachangeToGCSTsvOperator

gcp_conn_id = "gcp_airflow"  # do not work bigquery api
gcp_conn_id = "gcp_airflow_v2"
# gcp_conn_id = "gcp_p2"
# bucket_name = "myairflowbucket"
bucket_name = "myairflowbucket_v2"

default_args = {
    "owner": "was",
    "gcp_conn_id": gcp_conn_id,
    "bucket_name": bucket_name,
}

project_id = "myairflow-333619"
dataset_id = "myairflow"
table_name = "exchange_nbu_v2"
table_schema = [
    {"name": "currency_id", "type": "INT64",
        "mode": "REQUIRED", "description": "ID валюты"},
    {"name": "currency_name", "type": "STRING",
     "mode": "REQUIRED", "description": "Название валюты"},
    {"name": "rate", "type": "FLOAT", "mode": "REQUIRED",
        "description": "Курс относительно ГРН"},
    {"name": "currency_code", "type": "STRING",
        "mode": "REQUIRED", "description": "Код валюты"},
    {"name": "date", "type": "DATE", "mode": "REQUIRED", "description": "Дата"},
]


with DAG("lecture_6_custom_provider",
         default_args=default_args,
         schedule_interval="@daily",
         start_date=pendulum.datetime(2021, 6, 10, tz="Europe/Kiev"),
         catchup=False,
         tags=["lecture_6"]
         ) as dag:

    load_exchange_data_to_gcs_custom_operator = LoadExachangeToGCSTsvOperator(
        task_id="load_exchange_data_to_gcs_custom_operator",
        date_to="{{ds_nodash}}",
        bucket_name=bucket_name,
        object_name="rates/nbu_2/{{ds_nodash}}.tsv.gz",
        gzip=True
    )

    upload_to_bigquery = GCSToBigQueryOperator(
        task_id="upload_to_bigquery",
        bigquery_conn_id=gcp_conn_id,
        google_cloud_storage_conn_id=gcp_conn_id,
        bucket=bucket_name,
        source_objects=["rates/nbu_2/{{ds_nodash}}.tsv.gz"],
        source_format="CSV",                                # формат файла
        field_delimiter="\t",                               # разделить файла
        compression="GZIP",                                 # файл сжат алгоритмом gzip
        skip_leading_rows=1,                                # пропускаем заголовки
        schema_fields=table_schema,
        destination_project_dataset_table=f"{dataset_id}.{table_name}" + \
        "_{{ds_nodash}}",
        # создать таблицу, если она отсутвует https://cloud.google.com/bigquery/docs/reference/rest/v2/Job
        create_disposition="CREATE_IF_NEEDED",
        # перед загрузкой данных очистить таблицу
        write_disposition="WRITE_TRUNCATE",
    )

    load_exchange_data_to_gcs_custom_operator >> upload_to_bigquery
